# Seen Core

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-core/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-core/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6063f99bfb0f4741b7087fa5d29ffca3)](https://www.codacy.com/app/seen/seen-core?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-core&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-core/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-core/commits/develop)