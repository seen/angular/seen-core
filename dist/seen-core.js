/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/*
 * Base module of seen
 */
angular.module('seen-core', []);



/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('seen-core')

/**
 * @ngdoc Factories
 * @name PaginatedCollection
 * @description The result of search
 * 
 * 
 * @see QueryParameter
 */
.factory('PaginatedCollection', function(SeenObject) {

	var list = function() {
		SeenObject.apply(this, arguments);
	};

	list.prototype = new SeenObject();

	/**
	 * Check if there is more page in collection
	 * 
	 * @memberof PaginatedCollection
	 * @return {boolean} true if there exists more page
	 */
	list.prototype.hasMore = function() {
		return (this.current_page < this.page_number);
	};
	/**
	 * Check if it is the first page
	 * 
	 * @memberof PaginatedCollection
	 * @return {boolean} true if this is the first page
	 */
	list.prototype.isFirst = function() {
		return this.current_page === 1;
	};

	/**
	 * Gets next page index
	 * 
	 * @memberof PaginatedCollection
	 * @return {integer} index of the next page
	 */
	list.prototype.getNextPageIndex = function() {
		return this.current_page + 1;
	};

	/**
	 * Gets next page index
	 * 
	 * @memberof PaginatedCollection
	 * @return {integer} index of the previous page
	 */
	list.prototype.getPreviousPageIndex = function() {
		return this.current_page - 1;
	};
	return list;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('seen-core')

/**
 * @ngdoc Factories
 * @name QueryParameter
 * @description Manages parameters of a query
 * 
 * بسیاری از داده‌هایی که در سیستم موجود است به صورت صفحه بندی شده در اختیار
 * کاربران قرار می‌گیرد. در این بخش ابزارهایی برای کار با صفحه بندی ارائه شده
 * است.
 * 
 * 
 * از جمله خصوصیاتی که می‌توان در این ساختار قرار داد عبارتند از:
 * 
 * @attr {string} _px_q متن مورد جستجو در فیلدهای مختلف
 * @attr {Integer} _px_p شماره صفحه مورد نظر از فهرست صفحه‌بندی شده
 * @attr {Integer} _px_ps تعداد آیتم‌های موجود در هر صفحه
 * @attr {string} _px_fk نام خصوصیتی که برای فیلتر کردن مورد استفاده قرار
 *       می‌گیرد
 * @attr {string} _px_fv مقداری مورد نظر برای خصوصیتی که بر اساس آن فیلتر انجام
 *       می‌شود.
 * @attr {string} _px_sk نام خصوصیتی که فهرست باید بر اساس آن مرتب شود.
 * @attr {string} _px_so ترتیب مرتب‌سازی، اینکه مرتب‌سازی به صورت صعودی باشد یا
 *       نزولی
 * 
 */
.factory('QueryParameter', function() {
	var pagParam = function() {
		// init
		this.param = {};
		this.filterMap = {};
		this.sortMap = {};
		
		this._init_filters = function(){
			var obj = this.filterMap;
			var keys = Object.keys(obj);
			this.param['_px_fk[]'] = [];
			this.param['_px_fv[]'] = [];
			for(var i = 0; i < keys.length; i++){
				var key = keys[i];
				var values = obj[key];
				for(var j = 0; j < values.length; j++){
					var value = values[j];
					this.param['_px_fk[]'].push(key);
					this.param['_px_fv[]'].push(value);	
				}
			}
		};
		
		this._init_sorts = function(){
			var obj = this.sortMap;
			this.param['_px_sk[]'] = Object.keys(obj);
			// this.param['_px_so[]'] = Object.values(obj);
			this.param['_px_so[]'] = [];
			for(var index=0; index<this.param['_px_sk[]'].length; index++){
				var key = this.param['_px_sk[]'][index];
				this.param['_px_so[]'][index] = obj[key];
			}
		};
	};

	pagParam.prototype = {
		setSize : function(size) {
			this.param._px_ps = size;
			return this;
		},
		setQuery : function(query) {
			this.param._px_q = query;
			return this;
		},
		/**
		 * تعیین صفحه مورد نظر
		 * 
		 * این فراخوانی صفحه‌ای را تعیین می‌کند که مورد نظر کاربر است. برای
		 * نمونه اگر صفحه دوم از یک کاوش مد نظر باشد باید مقدار یک به عنوان
		 * ورودی این تابع استفاده شود.
		 * 
		 * اندیس تمام صفحه‌ها از صفر شروع می‌شود. بنابر این صفحه اول اندیس صفر و
		 * صفحه دوم اندیس یک دارد.
		 * 
		 * @param int
		 *            $page شماره صفحه
		 * @return QueryParameter خود شئی به عنوان خروجی برگردانده می‌شود.
		 */
		setPage : function($page) {
			this.param._px_p = $page;
			return this;
		},
		nextPage : function() {
			if(!this.param._px_p){
				this.param._px_p = 1;
			}
			this.param._px_p += 1;
			return this;
		},

		setOrder : function($key, $order) {
			if(!$order){				
				this.removeSorter($key, $order);
			}else{				
				this.addSorter($key, $order);
			}
			this._init_sorts();
			return this;
		},
		addSorter : function($key, $order){
			if(!$order){
				return this;
			}
			this.sortMap[$key] = $order;
			this._init_sorts();
			return this;
		},
		removeSorter : function($key){
			delete this.sortMap[$key];
			this._init_sorts();
			return this;
		},
		clearSorters: function(){
			this.sortMap = {};
		},
		
		/**
		 * Sets new filter and remove all old values
		 * 
		 * @memberof QueryParameter
		 */
		setFilter : function($key, $value) {
			if(!angular.isDefined($value)){				
				this.removeFilter($key, $value);
			}else{
				this.filterMap[$key] = [];
				this.addFilter($key, $value);
			}
			this._init_filters();
			return this;
		},
		
		addFilter : function($key, $value){
			if(!angular.isDefined($value)){				
				return this;
			}
			if(!angular.isArray(this.filterMap[$key])){
				this.filterMap[$key] = [];
			}
			this.filterMap[$key].push($value);
			this._init_filters();
			return this;
		},
		
		removeFilter : function($key){
			delete this.filterMap[$key];
			this._init_filters();
			return this;
		},
		
		/**
		 * Removes all filter options
		 * 
		 * @memberof QueryParameter
		 */
		clearFilters: function(){
			this.filterMap = {};
		},
		
		getParameter : function() {
			return this.param;
		},
		
		/**
		 * پارامترهای اضافه
		 * 
		 * در برخی از کاربردها نیاز به ارسال پارامترهای بیشتری به سرور هست. این
		 * فراخوانی امکان اضافه کردن پارامترهای اضافه را فراهم می‌کند.
		 * 
		 * @memberof QueryParameter
		 * @since 1.0.2
		 * 
		 * @param Object
		 *            value
		 * @param String
		 *            key کلید پارامتر مورد نظر
		 * @return خود موجودیت
		 */
		put : function(key, value) {
			this.param[key] = value;
			return this;
		},

		/**
		 * دسترسی به یک پارامترها خاص
		 * 
		 * این فراخوانی برای دسترسی به یک پارامتر خواص در نظر گرفته شده. این
		 * پارامترها معمولا به صورت اضافه برای سرور ارسال می‌شوند.
		 * 
		 * @memberof QueryParameter
		 * @since 1.0.2
		 * 
		 * @param String
		 *            key کلید پارامتر مورد نظر
		 * @return مقدار معادل با کلید
		 */
		get : function(key) {
			return this.param[key];
		}

	};
	return pagParam;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('seen-core')
/**
 * @ngdoc factory
 * @name SeenObject
 * @description مهم‌ترین موجودیت در سیستم است.
 *
 * @attr {Integer} id
 * شناسه موجودیت را تعیین می‌کند.
 *
 * @example
 *   Usage:
 *   <map MAP_OPTIONS_OR_MAP_EVENTS ..>
 *     ... Any children directives
 *   </map>
 *
 *   <map center="[40.74, -74.18]" on-click="doThat()">
 *   </map>
 *
 *   <map geo-fallback-center="[40.74, -74.18]" zoom-to-inlude-markers="true">
 *   </map>
 */
.factory('SeenObject', function() {
	/**
	 * این فراخوانی یک نمونه جدید از این موجودیت ایجاد کرده و مقادیر داده ورودی را به عنوان داده‌های
	 * این موجودیت قرار می‌دهد.
	 *
	 * @memberof PObject
	 * @param {data} ساختار داده‌ای موجودیت مورد نظر
	 */
	var pObject = function(data) {
		if (data) {
			this.setData(data);
		}
	};
	pObject.prototype = {
			/**
			 * داده‌های دریافتی را تعیین می‌کند
			 *
			 * @memberof PObject
			 * @param {data} ساختار داده‌ای اولیه
			 */
			setData : function(data) {
				angular.extend(this, data);
			},
			/**
			 * تعیین می‌کند که آیا ساختارهای داده‌ای نشان دارند. زمانی که یک ساختار
			 * داده‌ای شناسه معتبر داشته باشد و سمت کارگذار ذخیره شده باشد به عنوان
			 * یک داده نشان دار در نظر گرفته می‌شود. در غیر این صورت داده نا معتبر بوده و نباید در
			 * پردازش‌ها در نظر گرفته شود.
			 *
			 * نمونه‌ای از کاربردهای این فراخونی تعیین حالت کاربر است. در صورتی که خروجی این
			 * فراخوانی مقدار درستی باشد به معنی نا معتبر بودن کاربر است.
			 *
			 * زمانی که دوره زمانی زیادی از یک موجودیت گذشته باشد و با سرور هماهنگ نشده باشد نیز
			 * مقدار این تابع درستی خواهد بود از این رو سرویس‌ها باید این مدل داده‌ها را نادیده بگیرند. این
			 * روش در مدیریت کش کاربرد دارد.
			 *
			 * @memberof PObject
			 * @returns {Boolean} معتبر بودن ساختار داده
			 */
			isAnonymous : function() {
				return !(this.id && this.id > 0);
			},

			/**
			 * تعیین می‌کند که آیا موجودیت منقضی شده یا نه
			 *
			 * @memberof PObject
			 * @returns {Boolean} معتبر بودن ساختار داده
			 */
			isExpired : function() {
				// XXX: maso, 1395: check aot time
				return false;
			}
	};
	return pObject;
});

/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * @ngdoc Modules
 * @name seen
 * @description Manages basics of seen API
 * 
 * 
 */
var seen = (function(){

    var SeenStaticResourceCatch = function () {
        // TODO: maso, add options
        this.repository = {};
    };
    SeenStaticResourceCatch.prototype.has = function (url) {
        return (url in this.repository);
    };
    SeenStaticResourceCatch.prototype.get = function (url) {
        return this.repository[url];
    };
    SeenStaticResourceCatch.prototype.put = function (url, object) {
        this.repository[url] = object;
    };
    SeenStaticResourceCatch.prototype.clean = function () {
        this.repository = {};
    };


    function capitalizeFirstLetter(name) {
        return name.charAt(0).toUpperCase() + name.slice(1);
    }
    /**
     * Return plural name of the input name. 
     * 
     * Revert convert plural name to singular
     */
    function pluralName(name, revert){
        var plural = {
                '(quiz)$'               : '$1zes',
                '^(ox)$'                : '$1en',
                '([m|l])ouse$'          : '$1ice',
                '(matr|vert|ind)ix|ex$' : '$1ices',
                '(x|ch|ss|sh)$'         : '$1es',
                '([^aeiouy]|qu)y$'      : '$1ies',
                '(hive)$'               : '$1s',
                '(?:([^f])fe|([lr])f)$' : '$1$2ves',
                '(shea|lea|loa|thie)f$' : '$1ves',
                'sis$'                  : 'ses',
                '([ti])um$'             : '$1a',
                '(tomat|potat|ech|her|vet)o$': '$1oes',
                '(bu)s$'                : '$1ses',
                '(alias)$'              : '$1es',
                '(octop)us$'            : '$1i',
                '(ax|test)is$'          : '$1es',
                '(us)$'                 : '$1es',
                '([^s]+)$'              : '$1s'
        };

        var singular = {
                '(quiz)zes$'             : '$1',
                '(matr)ices$'            : '$1ix',
                '(vert|ind)ices$'        : '$1ex',
                '^(ox)en$'               : '$1',
                '(alias)es$'             : '$1',
                '(octop|vir)i$'          : '$1us',
                '(cris|ax|test)es$'      : '$1is',
                '(shoe)s$'               : '$1',
                '(o)es$'                 : '$1',
                '(bus)es$'               : '$1',
                '([m|l])ice$'            : '$1ouse',
                '(x|ch|ss|sh)es$'        : '$1',
                '(m)ovies$'              : '$1ovie',
                '(s)eries$'              : '$1eries',
                '([^aeiouy]|qu)ies$'     : '$1y',
                '([lr])ves$'             : '$1f',
                '(tive)s$'               : '$1',
                '(hive)s$'               : '$1',
                '(li|wi|kni)ves$'        : '$1fe',
                '(shea|loa|lea|thie)ves$': '$1f',
                '(^analy)ses$'           : '$1sis',
                '((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$': '$1$2sis',        
                '([ti])a$'               : '$1um',
                '(n)ews$'                : '$1ews',
                '(h|bl)ouses$'           : '$1ouse',
                '(corpse)s$'             : '$1',
                '(us)es$'                : '$1',
                's$'                     : ''
        };

        var irregular = {
                'move'   : 'moves',
                'foot'   : 'feet',
                'goose'  : 'geese',
                'sex'    : 'sexes',
                'child'  : 'children',
                'man'    : 'men',
                'tooth'  : 'teeth',
                'person' : 'people'
        };

        var uncountable = [
            'sheep', 
            'fish',
            'deer',
            'moose',
            'series',
            'species',
            'money',
            'rice',
            'information',
            'equipment'
            ];

        // save some time in the case that singular and plural are the same
        if(uncountable.indexOf(name.toLowerCase()) >= 0){
            return name;
        }

        // check for irregular forms
        var pattern, replace;
        for(var irName in irregular){
            if(!revert){
                pattern = new RegExp(irregular[irName]+'$', 'i');
                replace = irName;
            } else{ 
                pattern = new RegExp(irName+'$', 'i');
                replace = irregular[irName];
            }
            if(pattern.test(name)){
                return name.replace(pattern, replace);
            }
        }

        var array;
        if(!revert) {
            array = plural;
        } else {
            array = singular;
        }

        // check for matches using regular expressions
        for(var reg in array){
            pattern = new RegExp(reg, 'i');
            if(pattern.test(name)){
                return name.replace(pattern, array[reg]);
            }
        }

        if(!revert) {
            return name+'s';
        } else {
            return name;
        }
    }

    function singularName(name){
        return pluralName(name, true);
    }

    /*
     * Converts first litter to lower case
     */
    function jsLcfirst(string) 
    {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }

//  /*
//  * Converts first litter to uper case
//  */
//  function jsUcfirst(string) 
//  {
//  return string.charAt(0).toUpperCase() + string.slice(1);
//  }

    /**
     * 
     * @ngInject
     */
    function putObject($config, $http, $httpParamSerializerJQLike, $injector) {
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        // update model
        $config.headers = {
                'Content-Type' : 'application/x-www-form-urlencoded'
        };		
        $config.method = $config.method || 'POST';
        var Factory = $injector.get($config.factory || $config.name);
        return function(objectData) {
            $config.url = Mustache.render(urlTemplate, this);
            // Check if type is file
            if(objectData instanceof File){
                var formdata = new FormData();
                formdata.append('file', objectData);
                return $http.post($config.url, formdata, {
                    transformRequest : angular.identity,
                    headers : {
                        'Content-Type' : undefined
                    }
                });
            }          
            $config.data = $httpParamSerializerJQLike(objectData);
            return $http($config)//
            .then(function(res) {
                var item = res.data;
                if(Factory){
                    item = new Factory(item);
                }
                return item;
            });
        };
    }

    /**
     * 
     * @ngInject
     */
    function deleteObjects($config, $http) {
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'DELETE';
        return function(queryParameter) {
            if (queryParameter) {
                $config.params = queryParameter.getParameter();
            } else {
                $config.params = {};
            }
            $config.url = Mustache.render(urlTemplate, this);
            return $http($config)//
            .then(function(res) {
                return res.data;
            });
        };
    }
    /**
     * 
     * @ngInject
     */
    function deleteObjectById($config, $http) {
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'DELETE';
        return function(id) {
            $config.url = Mustache.render(urlTemplate, this);
            $config.url = $config.url + '/' + id;
            return $http($config)//
            .then(function(res) {
                return res.data;
            });
        };
    }

    /**
     * 
     * @ngInject
     */
    function deleteChildObject($config, $http) {
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'DELETE';
        return function(object) {
            $config.url = Mustache.render(urlTemplate, this) + '/' + object.id;
            return $http($config);
        };
    }
    /**
     * 
     * @ngInject
     */
    function putObjects($config, $http, $injector, PaginatedCollection) {
        // update model
        $config.method = $config.method || 'POST';
        var Factory = $injector.get($config.factory || $config.name);
        return function(objectData) {
            $config.data = objectData;
            return $http($config)//
            .then(function(res) {
                var page = new PaginatedCollection(res.data);
                var items = [];
                for (var i = 0; i < page.items.length; i++) {
                    var item = page.items[i];
                    if(Factory) {
                        item = new Factory(item);
                    }
                    items.push(item);
                }
                page.items = items;
                return page;
            });
        };
    }

    /**
     * Generate a method to get list of objects
     * 
     * @ngInject
     */
    function getObjects($config, $http, PaginatedCollection, $injector) {
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'GET';

        var Factory = $injector.get($config.factory || $config.name);

        return function(queryParameter) {
            if (queryParameter) {
                $config.params = queryParameter.getParameter();
            } else {
                $config.params = {};
            }
            $config.url = Mustache.render(urlTemplate, this);
            return $http($config)//
            .then(function(res) {
                var page = new PaginatedCollection(res.data);
                var items = [];
                for (var i = 0; i < page.items.length; i++) {
                    var item = page.items[i];
                    if(Factory && !$config.params.graphql) {
                        item = new Factory(item);
                    }
                    items.push(item);
                }
                page.items = items;
                return page;
            });
        };
    }



    /**
     * Generates a method to get an object
     * 
     * @ngInject
     */
    function getObject($config, $http, $injector) {
        // Cache templates
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        // update model
        $config.method = $config.method || 'GET';
        var Factory = $injector.get($config.factory || $config.name);
        return function(id, requestParams) {
            if (!requestParams) {
                requestParams = {};
            }
            $config.url = Mustache.render(urlTemplate, _.merge(this, {
                params:{
                    id: id
                }
            }));
            $config.params = requestParams;
            return $http($config)//
            .then(function(res) {
                var obj = res.data;
                if(Factory) {
                    obj = new Factory(res.data);
                }
                return obj;
            });
        };
    }

    /**
     * Updates an object
     * 
     * @ngInject
     */
    function postObject($config, $http, $httpParamSerializerJQLike) {
        // Cache templates
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        // update model
        $config.headers = {
                'Content-Type' : 'application/x-www-form-urlencoded'
        };		
        $config.method = $config.method || 'POST';
        return function(objectData) {
            $config.url = Mustache.render(urlTemplate, this);
            $config.data = $httpParamSerializerJQLike(objectData || this);
            var scope = this;
            return $http($config)//
            .then(function(res) {
                if(angular.isFunction(scope.setData)){
                    scope.setData(res.data);
                    return scope;
                }
                return res.data;
            });
        };
    }

    /*
     * Deletes an objects
     * 
     * @ngInject
     */
    function deleteObject($config, $http) {
        // Cache templates
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'DELETE';
        return function() {
            $config.url = Mustache.render(urlTemplate, this);
            var scope = this;
            return $http($config)//
            .then(function() {
                return scope;
            });
        };
    }

    /*
     * @ngInject
     */
    function getObjectSchema($q, $config, $http, $catch){
        // Cache templates
        var urlTemplate = $config.url + '/schema';
        Mustache.parse(urlTemplate, ['{', '}']);
        // update model
        $config.method = $config.method || 'GET';
        return function(requestParams) {
            if (!requestParams) {
                requestParams = {};
            }
            $config.url = Mustache.render(urlTemplate, this);
            if($catch.has($config.url)){
                return $q.when($catch.get($config.url));
            }
            $config.params = requestParams;
            var promise = $http($config)//
            .then(function(res) {
                $catch.put($config.url, res.data);
                return res.data;
            });
            $catch.put($config.url, promise);
            return promise;
        };
    }

    /*
     * Generate download function
     * 
     * @ngInject
     */
    function downloadResource($config, $http){
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'GET';
        return function(){
            $config.url = Mustache.render(urlTemplate, this);
            return $http($config)
            .then(function(res) {
                return res.data;
            });
        };
    }

    /*
     * Generate upload function
     * 
     * @ngInject
     */
    function uploadResource($config, $http){
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'POST';
        return function(newValue) {
            // TODO: maso, 2018: check if newValue is a file.
            $config.url = Mustache.render(urlTemplate, this);
            var promiss;
            if(!(newValue instanceof File)){
                $config.data = newValue;
                promiss = $http($config);
            } else {
                var formdata = new FormData();
                formdata.append('file', newValue);
                promiss = $http.post($config.url, formdata, {
                    transformRequest : angular.identity,
                    headers : {
                        'Content-Type' : undefined
                    }
                });
            }
            // update the object
            var scope = this;
            return promiss.then(function(result) {
                scope.setData(result.data);
                return scope;
            });
        };
    }

    /*
     * Generate delete resource function
     * 
     * @ngInject
     */
    function deleteResource($config, $http){
        var urlTemplate = $config.url;
        Mustache.parse(urlTemplate, ['{', '}']);
        $config.method = $config.method || 'DELETE';
        return function() {
            $config.url = Mustache.render(urlTemplate, this);
            return $http($config)
            .then(function(res) {
                return res.data;
            });
        };
    }

    // Default object
    var SObject = function(data) {
        if (data) {
            this.setData(data);
        }
    };
    SObject.prototype = {
            setData : function(data) {
                angular.extend(this, data);
            },
            isAnonymous : function() {
                return !(this.id && this.id > 0);
            },		
            isExpired : function() {
                // NOTE: maso, 2018: to be compatible with pluf
                return false;
            }
    };


    /**
     * 
     * Builders
     * 
     * this feature allow users to creates factories easy and fast.
     * 
     * types:
     * <ul>
     * <li>collection</li>
     * <li>item</li>
     * <li>binary</li>
     * <li>state</li>
     * </ul>
     * 
     * Here is an example
     * 
     * 
     * @example <caption>Creates a factory</caption> <code><pre>
     * var factories = [ {
     *     name : 'Content',
     *     path : '/api/v2/cms/contents',
     *     download : {
     *         type : 'binary',
     *         path : '/download'
     *     }
     * } ];
     * for (var i = 0; i &lt; factories.length; i++) {
     *     module.factory(factory.name, seen.factory(factory));
     * }
     * </pre></code>
     * 
     * @memberof seen
     * @param config
     *            {object} configuration to build new factory
     * 
     */
    function createFactory(config){

        /**
         * Generates new instance of factory
         * 
         * @ngInject
         */
        function _factory_generator($injector){
            var factory = function(){
                SObject.apply(this, arguments);

                if(angular.isArray(config.resources)){
                    for(var i = 0; i < config.resources.length; i++){
                        var resource = config.resources[i];
                        // TODO: maso, 2018: assert name not null
                        var name = capitalizeFirstLetter(resource.name);
                        var pName = pluralName(name);
                        var sName = singularName(name);

                        var locals = {
                                '$config': _.assign({}, resource, {
                                    url: config.url + '/{id}' + resource.url
                                }),
                                '$catch' : sysCatch,
                        };

                        if(resource.type === 'collection') {
                            // Check if is created
                            if(angular.isFunction('get'+pName)){
                                continue;
                            }
                            factory.prototype['get'+pName] = $injector.invoke(getObjects, factory, _.cloneDeep(locals));
                            factory.prototype['get'+sName] = $injector.invoke(getObject, factory, {
                                '$config': _.assign({}, resource, {
                                    url: config.url + '/{id}' + resource.url + '/{params.id}'
                                }),
                                '$catch' : sysCatch,
                            });
                            factory.prototype['delete'+pName] = $injector.invoke(deleteObjects, factory, _.cloneDeep(locals));
                            factory.prototype['delete'+sName] = $injector.invoke(deleteChildObject, factory, _.cloneDeep(locals));
                            factory.prototype['put'+pName] = $injector.invoke(putObjects, factory, _.cloneDeep(locals));
                            factory.prototype['put'+sName] = $injector.invoke(putObject, factory, _.cloneDeep(locals));
                            factory.prototype[jsLcfirst(sName) + 'Schema'] = $injector.invoke(getObjectSchema, factory, {
                                '$config': _.cloneDeep(locals.$config),
                                '$catch' : schemaResourceCatch,
                            });
                        } else if (resource.type === 'binary') {
                            // Check if is created
                            if(angular.isFunction('download'+sName)){
                                continue;
                            }
                            factory.prototype['download'+sName] = $injector.invoke(downloadResource, factory, _.cloneDeep(locals));
                            factory.prototype['upload'+sName] = $injector.invoke(uploadResource, factory, _.cloneDeep(locals));
                            factory.prototype['delete'+sName] = $injector.invoke(deleteResource, factory, _.cloneDeep(locals));
                        }
                    }
                }
            };
            factory.prototype = new SObject();

            factory.prototype.update = $injector.invoke(postObject, factory, {
                '$config': _.merge({}, config, {
                    url: config.url + '/{id}'
                }),
                '$catch' : sysCatch,
            });
            factory.prototype.delete = $injector.invoke(deleteObject, factory, {
                '$config': _.merge({}, config, {
                    url: config.url + '/{id}'
                }),
                '$catch' : sysCatch,
            });
            return factory;
        }

        return _factory_generator;
    }

    /**
     * Creates new service and return as result
     * 
     * @memberof seen
     * @param config
     *            {object}
     */
    function createService(config) {
        config.resources = config.resources || [];

        // TODO inject services
        /*
         * @ngInject
         */
        var newService = function($injector){
            var scope = this;
            // TODO: maso, 2018: check and init service
            for(var i = 0; i < config.resources.length; i++){
                var resource = config.resources[i];
                // TODO: maso, 2018: assert name not null
                var name = capitalizeFirstLetter(resource.name);
                var pName = pluralName(name);
                var sName = singularName(name);

                scope['get'+pName] = $injector.invoke(getObjects, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : sysCatch,
                });
                scope['get'+sName] = $injector.invoke(getObject, this, {
                    '$config': _.assign({}, resource, {
                        url: resource.url + '/{params.id}'
                    }),
                    '$catch' : sysCatch,
                });
                scope['delete'+pName] = $injector.invoke(deleteObjects, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : sysCatch,
                });
                scope['delete'+sName] = $injector.invoke(deleteObjectById, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : sysCatch,
                });
                scope['put'+pName] = $injector.invoke(putObjects, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : sysCatch,
                });
                scope['put'+sName] = $injector.invoke(putObject, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : sysCatch,
                });
                scope[jsLcfirst(sName) + 'Schema'] = $injector.invoke(getObjectSchema, this, {
                    '$config': _.assign({}, resource),
                    '$catch' : schemaResourceCatch,
                });
            }
        };
        return newService;
    }


    // api
    var sysCatch = null; // XXX: maso, 2018: add cache manager
    var schemaResourceCatch = new SeenStaticResourceCatch();
    return {
        factory: createFactory,
        service: createService,

        // Util
        pluralName: pluralName,
        singularName: singularName
    };
}());


window.seen = seen;

/*
 * XXX: new feature of seen
 * 
 * Cache objects
 * 
 * By adding cache of object, we can improve our performance.
 * 
 * SEE: - https://github.com/rsms/js-lru
 * 
 */

