/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//Adding test service
angular.module('testModule', ['seen-core'])
.factory('Book', seen.factory({
	url: '/api/v2/books',
}))
.factory('Page', seen.factory({
	url: '/api/v2/pages',
}))
.factory('Fish', seen.factory({
	url: '/api/v2/fish',
}))
.service('$myTest', seen.service({
	resources: [{
		name: 'Book',
		url: '/api/v2/books',
		type: 'collection'
	},{
		name: 'Page',
		url: '/api/v2/pages',
		type: 'collection'
	}]
}));

describe('Seen build service', function() {

	var $myTest;
	var $rootScope;
	var $httpBackend;
	
	beforeEach(function() {
		module('testModule');
		inject(function(_$myTest_, _$rootScope_, _$httpBackend_) {
			$myTest = _$myTest_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
		});
	});

	// check to see if it has the expected function
	it('should attaches a service', function() {
		expect($myTest).not.toBe(null);
	});

	// check to see if it has the expected function
	it('should attaches a book function into service', function() {
		// auto created function
		expect(angular.isFunction($myTest.getBooks)).toBe(true);
		expect(angular.isFunction($myTest.getBook)).toBe(true);
		expect(angular.isFunction($myTest.deleteBooks)).toBe(true);
		expect(angular.isFunction($myTest.putBook)).toBe(true);
		expect(angular.isFunction($myTest.putBooks)).toBe(true);
		expect(angular.isFunction($myTest.bookSchema)).toBe(true);
	});

	// check to see if it has the expected function
	it('should attaches a page function into service', function() {
		// auto created function
		expect(angular.isFunction($myTest.getPages)).toBe(true);
		expect(angular.isFunction($myTest.getPage)).toBe(true);
		expect(angular.isFunction($myTest.deletePages)).toBe(true);
		expect(angular.isFunction($myTest.putPage)).toBe(true);
		expect(angular.isFunction($myTest.putPages)).toBe(true);
		expect(angular.isFunction($myTest.pageSchema)).toBe(true);
	});
	

	it('should call /api/v2/books/{id} to get', function(done) {
		$myTest.getBook(1)//
		.then(function(object) {
			expect(object).not.toBeNull();
			expect(object.isAnonymous()).toBe(false);
			expect(object.isExpired()).toBe(false);
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});

		$httpBackend//
		.expect('GET', '/api/v2/books/1')//
		.respond(200, {
			id:1
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/books/{id} to get page', function(done) {
		$myTest.getPage(101)//
		.then(function(object) {
			expect(object).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});
		
		$httpBackend//
		.expect('GET', '/api/v2/pages/101')//
		.respond(200, {
			id:101
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});

});
