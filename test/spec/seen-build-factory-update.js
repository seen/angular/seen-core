/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//Adding test service
angular.module('testModuleFactory', ['seen-core'])
.factory('MyFactory', seen.factory({
	url: '/api/v2/books',
	sub:[{
		name: 'Author',
		url: '/auther',
		type: 'item'
	}]
}));

describe('Seen build factory', function() {
	var $rootScope;
	var $httpBackend;
	var MyFactory;
	
	beforeEach(function() {
		module('testModuleFactory');
		inject(function(_$rootScope_, _$httpBackend_, _MyFactory_) {
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
			MyFactory = _MyFactory_;
		});
	});

	it('should call /api/v2/books/{id} to update', function(done) {
		// auto created function
		var factory = new MyFactory({
			id: 1
		});
		
		factory.update()//
		.then(function(object) {
			expect(object).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});

		$httpBackend//
		.expect('POST', '/api/v2/books/1')//
		.respond(200, {
			id:1
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
});
