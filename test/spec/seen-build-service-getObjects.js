/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//Adding test service
angular.module('testModule124', ['seen-core'])
.factory('Book', seen.factory({
	url: '/api/v2/books',
}))
.factory('Page', seen.factory({
	url: '/api/v2/pages',
}))
.factory('Property', seen.factory({
    url: 'api/v2/properties'
}))
.service('$myTest', seen.service({
	resources: [{
		name: 'Book',
		url: '/api/v2/books',
		type: 'collection'
	},{
		name: 'Page',
		url: '/api/v2/pages',
		type: 'collection'
	},{
        name: 'Property',
        url: '/api/v2/properties',
        type: 'collection'
    }]
}));

describe('Seen build service', function() {

	var $myTest;
	var $rootScope;
	var $httpBackend;
	
	beforeEach(function() {
		module('testModule124');
		inject(function(_$myTest_, _$rootScope_, _$httpBackend_) {
			$myTest = _$myTest_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
		});
	});

	// check to see if it has the expected function
	it('should attaches a service', function() {
		expect($myTest).not.toBe(null);
	});

	it('should call /api/v2/books to get books', function(done) {
		$myTest.getBooks()//
		.then(function(object) {
			expect(object).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});

		$httpBackend//
		.expect('GET', '/api/v2/books')//
		.respond(200, {
			items:[]
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/pages to get pages', function(done) {
		$myTest.getPages()//
		.then(function(objects) {
			expect(objects).not.toBeNull();
			expect(objects.hasMore()).toBe(false);
			expect(objects.isFirst()).toBe(true);
			expect(objects.getNextPageIndex()).toBe(2);
			expect(objects.getPreviousPageIndex()).toBe(0);
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});
		
		$httpBackend//
		.expect('GET', '/api/v2/pages')//
		.respond(200, {
			current_page: 1,
			page_number: 0,
			items:[]
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/properties to get properties', function(done) {
        $myTest.getProperties()//
        .then(function(objects) {
            expect(objects).not.toBeNull();
            expect(objects.hasMore()).toBe(false);
            expect(objects.isFirst()).toBe(true);
            expect(objects.getNextPageIndex()).toBe(2);
            expect(objects.getPreviousPageIndex()).toBe(0);
            done();
        }, function() {
            expect(false).toBe(true);
            done();
        });
        
        $httpBackend//
        .expect('GET', '/api/v2/properties')//
        .respond(200, {
            current_page: 1,
            page_number: 0,
            items:[]
        });
        expect($httpBackend.flush).not.toThrow();
        $rootScope.$apply();
    });
	
});
