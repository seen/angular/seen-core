/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

//Adding test service
describe('Query parameter', function() {
	var QueryParameter;

	beforeEach(function() {
		module('seen-core');
		inject(function(_QueryParameter_) {
			QueryParameter = _QueryParameter_;
		});
	});

	it('should attache basic functions', function() {
		var param = new QueryParameter();
		expect(angular.isFunction(param.setSize)).toBe(true);
		expect(angular.isFunction(param.setQuery)).toBe(true);
		expect(angular.isFunction(param.setPage)).toBe(true);
		expect(angular.isFunction(param.nextPage)).toBe(true);
		expect(angular.isFunction(param.setOrder)).toBe(true);
		expect(angular.isFunction(param.addSorter)).toBe(true);
		expect(angular.isFunction(param.addSorter)).toBe(true);
		expect(angular.isFunction(param.clearSorters)).toBe(true);
		expect(angular.isFunction(param.setFilter)).toBe(true);
		expect(angular.isFunction(param.addFilter)).toBe(true);
		expect(angular.isFunction(param.removeFilter)).toBe(true);
		expect(angular.isFunction(param.clearFilters)).toBe(true);
		expect(angular.isFunction(param.getParameter)).toBe(true);
		expect(angular.isFunction(param.put)).toBe(true);
		expect(angular.isFunction(param.get)).toBe(true);
	});

	it('should put a custom key value', function(){
		var param = new QueryParameter();
		var key = 'key'+ Math.random();
		var value = 'value'+Math.random();

		param.put(key, value);
		expect(param.get(key)).toBe(value);
	});

	it('should set page size', function(){
		var param = new QueryParameter();
		var size = Math.random();
		param.setSize(size);
		expect(param.get('_px_ps')).toBe(size);
	});

	it('should set query', function(){
		var param = new QueryParameter();
		var value = 'query'+Math.random();
		param.setQuery(value);
		expect(param.get('_px_q')).toBe(value);
	});

	it('should set next page', function(){
		var param = new QueryParameter();
		param.nextPage();
		expect(param.get('_px_p')).toBe(2);
	});

	it('should set order', function(){
		var param = new QueryParameter();
		param.setOrder('id', 'a');
		expect(param.get('_px_sk[]')).not.toBe(null);
		expect(param.get('_px_so[]')).not.toBe(null);
	});

	it('should add order', function(){
		var param = new QueryParameter();
		param.setOrder('id', 'a');
		param.addSorter('key', 'd');
		expect(param.get('_px_sk[]')).not.toBe(null);
		expect(param.get('_px_so[]')).not.toBe(null);
	});

	it('should remove order by set', function(){
		var param = new QueryParameter();
		param.setOrder('id', 'a');
		param.setOrder('id');

		var sk = param.get('_px_sk[]');
		expect(angular.isDefined(sk.id)).toBe(false);
	});
	it('should add/remvoe order', function(){
		var param = new QueryParameter();
		param.addSorter('id', 'a');
		expect(param.get('_px_sk[]').indexOf('id')).not.toBe(-1);

		param.removeSorter('id');
		expect(param.get('_px_sk[]').indexOf('id')).toBe(-1);
	});



	it('should set filter', function(){
		var param = new QueryParameter();
		param.setFilter('id', 'a');
		expect(param.get('_px_fk[]')).not.toBe(null);
		expect(param.get('_px_fv[]')).not.toBe(null);
	});

	it('should remove filter by set', function(){
		var param = new QueryParameter();
		param.setFilter('id', 'a');
		expect(param.get('_px_fk[]')).not.toBe(null);
		expect(param.get('_px_fv[]')).not.toBe(null);
		expect(param.get('_px_fk[]').indexOf('id')).not.toBe(-1);

		param.setFilter('id');
		expect(param.get('_px_fk[]').indexOf('id')).toBe(-1);
	});

	it('should add/remove filter ', function(){
		var param = new QueryParameter();
		param.addFilter('id', 'a');
		expect(param.get('_px_fk[]')).not.toBe(null);
		expect(param.get('_px_fv[]')).not.toBe(null);
		expect(param.get('_px_fk[]').indexOf('id')).not.toBe(-1);

		param.removeFilter('id');
		expect(param.get('_px_fk[]').indexOf('id')).toBe(-1);
	});

	it('should add multi value filter ', function(){
		var param = new QueryParameter();
		param.addFilter('id', 'a');
		expect(param.get('_px_fk[]')).not.toBe(null);
		expect(param.get('_px_fv[]')).not.toBe(null);
		expect(param.get('_px_fk[]').indexOf('id')).not.toBe(-1);

		param.addFilter('id', 'a');
		{
			var keys = param.get('_px_fk[]');
			var count = 0;
			for(var i = 0; i < keys.length; i++){
				if(keys[i] === 'id'){
					count++;
				}
			}
			expect(count).toBe(2);
		}

		param.setFilter('id', 'a');
		{
			var keys = param.get('_px_fk[]');
			var count = 0;
			for(var i = 0; i < keys.length; i++){
				if(keys[i] === 'id'){
					count++;
				}
			}
			expect(count).toBe(1);
		}

		param.removeFilter('id');
		expect(param.get('_px_fk[]').indexOf('id')).toBe(-1);
	});
});
