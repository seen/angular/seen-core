/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

// Adding test service
angular.module('testModuleFactoryResources', ['seen-core'])
.factory('Book', seen.factory({
	url: '/api/v2/books',
	resources: [{
		name: 'Cover',
		url: '/cover',
		type: 'binary'
	}]
}))
.factory('Page', seen.factory({
	url: '/api/v2/pages',
}))
.factory('MyFactory', seen.factory({
	url: '/api/v2/myFactories',
	resources: [{
		name: 'Book',
		url: '/books',
		type: 'collection'
	},{
		name: 'Page',
		url: '/pages',
		type: 'collection'
	},{
		name: 'Content',
		url: '/content',
		type: 'binary'
	}]
}));

describe('Seen build factory', function() {

	var $rootScope;
	var $httpBackend;
	var MyFactory;
	var QueryParameter;
	
	beforeEach(function() {
		module('testModuleFactoryResources');
		inject(function(_$rootScope_, _$httpBackend_,_MyFactory_, _QueryParameter_) {
			MyFactory = _MyFactory_;
			QueryParameter = _QueryParameter_;
			$rootScope = _$rootScope_;
			$httpBackend = _$httpBackend_;
		});
	});

	// check to see if it has the expected function
	it('should attaches a factory', function() {
		expect(MyFactory).not.toBe(null);
	});

	// check to see if it has the expected function
	it('should attaches update and delete function into factory', function() {
		// auto created function
		var factory = new MyFactory();
		expect(angular.isFunction(factory.update)).toBe(true);
		expect(angular.isFunction(factory.delete)).toBe(true);
		

		expect(angular.isFunction(factory.getBooks)).toBe(true);
		expect(angular.isFunction(factory.deleteBooks)).toBe(true);
		expect(angular.isFunction(factory.putBook)).toBe(true);
		expect(angular.isFunction(factory.putBooks)).toBe(true);
		expect(angular.isFunction(factory.bookSchema)).toBe(true);
		

		expect(angular.isFunction(factory.getPages)).toBe(true);
		expect(angular.isFunction(factory.deletePages)).toBe(true);
		expect(angular.isFunction(factory.putPage)).toBe(true);
		expect(angular.isFunction(factory.putPages)).toBe(true);
		expect(angular.isFunction(factory.pageSchema)).toBe(true);
		
		expect(angular.isFunction(factory.downloadContent)).toBe(true);
		expect(angular.isFunction(factory.uploadContent)).toBe(true);
	});

	

	it('should call /api/v2/myFactories/1/books to getlist of books', function(done) {
		// auto created function
		var factory = new MyFactory({
			id: 1
		});
		var param = new QueryParameter();
		factory.getBooks(param)//
		.then(function(objects) {
			expect(objects).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});

		$httpBackend//
		.expect('GET', '/api/v2/myFactories/1/books')//
		.respond(200, {
			items:[]
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/myFactories/1/books to put a book', function(done) {
		// auto created function
		var factory = new MyFactory({
			id: 1
		});
		factory.putBook({})//
		.then(function(object) {
			expect(object).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});
		
		$httpBackend//
		.expect('POST', '/api/v2/myFactories/1/books')//
		.respond(200, {
			id: 1
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/myFactories/1/books to delete a book', function(done) {
		// auto created function
		var factory = new MyFactory({
			id: 1
		});
		factory.deleteBook({id:1})//
		.then(function(object) {
			expect(object).not.toBeNull();
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});
		
		$httpBackend//
		.expect('DELETE', '/api/v2/myFactories/1/books/1')//
		.respond(200, {
			id: 1
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
	
	it('should call /api/v2/myFactories/1/books to delete books', function(done) {
		// auto created function
		var factory = new MyFactory({
			id: 1
		});
		factory.deleteBooks()//
		.then(function() {
			done();
		}, function() {
			expect(false).toBe(true);
			done();
		});
		
		$httpBackend//
		.expect('DELETE', '/api/v2/myFactories/1/books')//
		.respond(200, {
			items: []
		});
		expect($httpBackend.flush).not.toThrow();
		$rootScope.$apply();
	});
});
